# Collection of Random Development Quotes and Pithy Software Explanations

## Frontend

### React

- We just think [React/Redux] is a very sane way to build a complex product made of smaller, modular apps.

- React makes it smooth to build the UI declaratively from meaningful (reusable) components, and [Redux] brings a sensible data flow to the mix[, along with top-notch development tools (e.g. ReduxDevTools, which enables state time-travel and granular control of the app state machine)]

### Testing

## State Management

### Redux

## Backend

### Django

### Testing

## TypeScript

## DevOps

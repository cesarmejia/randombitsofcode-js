/**
 * ref: https://github.com/jordanahaines/mywiki/blob/master/templates/react_ts_redux_component.tsx
 */

import React, { Component } from 'react';
//# RootState => an interface representing the application state type shape
import { RootState } from '../store/rootReducer.ts';
//# connect => HOC that gives a React component access to the redux store and action creators
import { connect } from 'react-redux';
//? Is that correct? Dispatch (capital D?) AnyAction (placeholder?)
import { bindActionCreators, Dispatch, AnyAction } from 'redux';

//# type annotation for functional & class component parameters
interface OwnProps {
  // No props
}

//# type annotation for class component parameters
interface OwnState {
  // No state
}

//# Need to review TS ReturnType and Component Type Annotation Rules (Classes vs Functions)
type MyComponentProps = ReturnType<typeof mapState> & ReturnType<typeof mapDispatch> & OwnProps;

//# Type annotation of a React Class Component connected to Redux store
class MyComponent extends Component<MyComponentProps, OwnState> {
  //? Like so???
  state: OwnState = {};
  render() {
    return <div className='task-list-item'>Placeholder</div>;
  }
}

//# mapState => a function that receives state as a parameter and returns a slice of state to be appended(merged?) to/with component props
const mapState = (state: RootState) => ({
  // Add state to map to props
});

//# mapDispatch => a function that receives dispatch as a parameter in order to enable the component
//# to dispatch action creators that will be processed by the rootReducer to generate an application
//# state change (?? technical language accurate ??)
const mapDispatch = (dispatch: Dispatch<AnyAction>) =>
  bindActionCreators(
    {
      // Add Action Creators
    },
    dispatch
  );

//# connector => HOC that will connect specified component to redux store
const connector = connect(mapState, mapDispatch);

export default connector(MyComponent);
